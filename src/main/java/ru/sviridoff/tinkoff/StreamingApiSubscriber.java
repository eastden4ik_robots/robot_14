package ru.sviridoff.tinkoff;

import org.jetbrains.annotations.NotNull;
import org.reactivestreams.example.unicast.AsyncSubscriber;
import ru.sviridoff.framework.logger.Logger;
import ru.tinkoff.invest.openapi.models.streaming.StreamingEvent;

import java.util.concurrent.Executor;

class StreamingApiSubscriber extends AsyncSubscriber<StreamingEvent> {

    private final Logger logger;

    StreamingApiSubscriber(@NotNull final Logger logger, @NotNull final Executor executor) {
        super(executor);
        this.logger = logger;
    }

    @Override
    protected boolean whenNext(final StreamingEvent event) {
        logger.info("Пришло новое событие из Streaming API\n" + event);

        return true;
    }

}
