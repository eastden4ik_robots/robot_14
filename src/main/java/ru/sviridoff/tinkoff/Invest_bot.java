package ru.sviridoff.tinkoff;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import ru.sviridoff.framework.logger.Logger;
import ru.sviridoff.framework.props.Props;
import ru.sviridoff.framework.props.UtilsProps;

import java.io.IOException;


public class Invest_bot {

    private static Logger logger = new Logger("Tinkoff investment.");
    private static UtilsProps utilsProps = new UtilsProps();
    private static Props props;

    static {
        try {
            props = new Props("./Tinkoff.properties");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {

        var TOKEN = (String) utilsProps.getParameter("TOKEN", props.getProperties());
        var SITE = (String) utilsProps.getParameter("SITE", props.getProperties());
        var sandboxMode = (Boolean) utilsProps.getParameter("MODE", props.getProperties());

        if (!sandboxMode) {
            HttpResponse<JsonNode> response = Unirest.get(SITE + "/trading/bonds/get?ticker=RU000A0JSGV0")
//                    .header("Authorization", "Bearer " + TOKEN)
                    .header("accept", "application/json")
//                    .connectTimeout(3)
                    .asJson();
            logger.success(SITE + "/trading/bonds/get?ticker=RU000A0JSGV0");
            logger.info(response.getBody().toPrettyString());
        }


    }

}
